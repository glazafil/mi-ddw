# import
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import csv


def cosine(query, data):
    distance = np.array(cosine_similarity(query, data)[0])
    distances_sorted = distance.argsort()[::-1] + 1
    return distances_sorted


def euclidean(query, data):
    distances = np.array(euclidean_distances(query, data))
    distances_sorted = distances.argsort()
    return distances_sorted[0]


def precision(recv, rel):
    # TP/len(rel)
    TP = 0
    FP = 0
    for doc in recv:
        doc += 1
        # print("Search [", doc, "] in ", rel)
        if doc in rel:
            TP += 1
        else:
            FP += 1
    # print("CNT: ", cnt)
    # print('')
    return TP / (TP + FP)


def recall(recv, rel):
    TP = 0
    for doc in recv:
        doc += 1
        if doc in rel:
            TP += 1
    return TP / len(rel)


def fm(prec, rec):
    if prec + rec == 0:
        return 0
    return 2 * (prec * rec) / (prec + rec)


# prepare corpus
corpus = []
for d in range(1400):
    f = open("./d/" + str(d + 1) + ".txt")
    corpus.append(f.read())
corpus.append('')

queries = []
for q in range(225):
    f = open("./q/" + str(q + 1) + ".txt")
    queries.append(f.read())

reference = []
for r in range(225):
    reference.append([])
    f = open("./r/" + str(r + 1) + ".txt")
    for line in f:
        reference[r].append(int(line))

# query => [binary_euclid, binary_cosine, tf_euclid, tf_cosine, tfidf_euclide, tfidf_cosine]
results_by_query = []
# query => [Bin euclid precision, Bin cosine precision, Bin euclid recall, Bin euclid recall, Bin euclid fm, Bin cosine fm + TF + TFIDF]
output_by_query = []

for i, query in enumerate(queries):
    print(i)
    corpus[1400] = query
    results_by_query.append([])
    output_by_query.append([])
    vectorizers = [CountVectorizer(binary=True), TfidfVectorizer(use_idf=False), TfidfVectorizer()]

    for vectorizer in vectorizers:
        count_array = vectorizer.fit_transform(corpus)
        q = count_array[len(corpus) - 1]
        data = count_array[0:len(corpus) - 1]
        euclid_res = euclidean(q, data)
        cosine_res = cosine(q, data)
        results_by_query[i].append(euclid_res)
        results_by_query[i].append(cosine_res)

    iterator = [1, 3, 5]

    for j in iterator:
        ref = reference[i]
        lenght = len(ref)
        prec_e = precision(results_by_query[i][j - 1][0:10], ref)
        rec_e = recall(results_by_query[i][j - 1][0:10], ref)
        fm_e = fm(prec_e, rec_e)
        prec_c = precision(results_by_query[i][j][0:10], ref)
        rec_c = recall(results_by_query[i][j][0:10], ref)
        fm_c = fm(prec_c, rec_c)
        output_by_query[i].append(prec_e)
        output_by_query[i].append(prec_c)
        output_by_query[i].append(rec_e)
        output_by_query[i].append(rec_c)
        output_by_query[i].append(fm_e)
        output_by_query[i].append(fm_c)


with open('output.csv', mode='w') as f:
    writer = csv.writer(f, delimiter=';')
    csv_row = ['Query',
               'Binary euclidean precision',
               'Binary cosine precision',
               'Binary euclidean recall',
               'Binary cosine recall',
               'Binary euclidean fmeasure',
               'Binary cosine fmeasure',
               'TF euclidean precision',
               'TF cosine precision',
               'TF euclidean recall',
               'TF cosine recall',
               'TF euclidean fmeasure',
               'TF cosine fmeasure',
               'TFIDF euclidean precision',
               'TFIDF cosine precision',
               'TFIDF euclidean recall',
               'TFIDF cosine recall',
               'TFIDF euclidean measure',
               'TFIDF cosine fmeasure'
               ]
    writer.writerow(csv_row)
    for i, q in enumerate(output_by_query, 1):
        csv_row = [i,
                   q[0],
                   q[1],
                   q[2],
                   q[3],
                   q[4],
                   q[5],
                   q[6],
                   q[7],
                   q[8],
                   q[9],
                   q[10],
                   q[11],
                   q[12],
                   q[13],
                   q[14],
                   q[15],
                   q[16],
                   q[17]
                   ]
        writer.writerow(csv_row)
