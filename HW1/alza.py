import scrapy
import time


def getrooturl(url):
    parts = url.split('/')
    del parts[-1]
    return '/'.join([str(i) for i in parts])


class BlogSpider(scrapy.Spider):
    name = 'blogspider'
    user_agent = 'DDW'
    download_delay = 1.0
    allowed_domains = ["alza.cz"]
    start_urls = ['https://www.alza.cz/macbook/18854758.htm']
    frontier = []
    crawled = []

    def parse(self, response):
        for title in response.css('.box'):
            url = title.css('.top .fb .name ::attr(href)').extract_first()
            request = scrapy.Request(response.urljoin(url), callback=self.parseProduct)
            request.meta['product'] = {}
            yield request

        next_page = response.css('.next ::attr(href)').extract()

        for link in next_page:
            yield scrapy.Request(response.urljoin(getrooturl(response.url) + '/' + link), callback=self.parse)

    def parseProduct(self, response):
        product = response.meta['product']
        name = response.css('h1 ::text').extract_first().strip()
        description = response.css('.nameextc span ::text').extract_first()
        price = response.css('span.price_withVat ::text').extract_first()
        price = price[:-2]
        photos = response.css('#galleryPreview .container .inner a.lightBoxImage ::attr(href)').extract()
        gallery = []
        for photo in photos:
            gallery.append(photo)
        vote = ({'value': '', 'reviews': []})
        vote['value'] = response.css('.blockReviewSummary .c12 ::text').extract_first().strip()
        for review in response.css('.blockUserReviews .userReview'):
            item = ({'author': review.css('.userName ::text').extract_first(), 'pros': [], 'cons': []})
            for pros in review.css('.positive ::text').extract():
                item['pros'].append(pros)
            for cons in review.css('.negative ::text').extract():
                item['cons'].append(cons)
            vote['reviews'].append(item)
        product.update({
            'name': name,
            'description': description,
            'price': price,
            'images': gallery,
            'review': vote
        })

        yield product
